# FriendsBookProject

The project highlights some of my work with Angular and typescript.
The project makes use of Angular material components.

Run `npm install` before starting the server.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
