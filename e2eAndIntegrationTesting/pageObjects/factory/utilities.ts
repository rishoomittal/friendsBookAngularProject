export class Utilities {

    public static GetDomElement(selectionValue:string, attributeName?: string) {
        (attributeName === null || attributeName === undefined) ? attributeName = "data-cy" : attributeName;
        return cy.get(`[${attributeName}="${selectionValue}"]`);
    }
}