import { SharedForm } from "../forms/sharedForm";
import { LoginPage } from "../generalUser/loginPage";

export class MainPage {

    public sharedForm: SharedForm;
    public loginPage: LoginPage;

    constructor() {
        this.loginPage = new LoginPage();
        this.sharedForm = new SharedForm();
    }
}