import { Utilities } from "../factory/utilities";

export class SharedForm {
    
    /*----------Get Input Field Elements----------------------------*/

    getFirstNameInputField() {
        return Utilities.GetDomElement('firstName');
    }

    getLastNameInputField() {
        return Utilities.GetDomElement('lastName');
    }

    getDateOfBirthInputField() {
        return Utilities.GetDomElement('dob');
    }

    getEmailIdInputField() {
        return Utilities.GetDomElement('emailId');
    }

    getPasswordInputField() {
        return Utilities.GetDomElement('passwordNewUser');
    }

    getConfirmPasswordInputField() {
        return Utilities.GetDomElement('confirmPassword');
    }

    getGenderInputField() {
        return Utilities.GetDomElement('gender');
    }

    /*----------Get Validations Error Message Elements----------------------------*/

    getFirstNameError() {
        return this.getErrorElement('parentfirstName');
    }

    getLastNameError() {
        return this.getErrorElement('parentlastName');
    }

    getDateOfBirthError() {
        return this.getErrorElement('parentDobDivPicker');
    }
    
    getPasswordError() {
        return this.getErrorElement('parentpasswordNewUser');
    }

    getEmailIdError() {
        return this.getErrorElement('parentemailId');
    }

    getConfirmPasswordError() {
        return this.getErrorElement('parentconfirmPassword');
    }

    getErrorElement(attributeValue: string) {
        return cy.get(`[data-cy="${attributeValue}"] .invalid-feedback`);
    }

    /*-------------------Form Button--------------------------------*/

    getSubmitButton() {
        return cy.get(`[data-cy="submitFormButton"]`);
    }

}