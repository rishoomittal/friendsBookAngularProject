/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {
      /**
       * Custom command to login a user and set token in local storage.
       */
      login(emailId: string, password: string): Chainable<Element>;
    }
  }