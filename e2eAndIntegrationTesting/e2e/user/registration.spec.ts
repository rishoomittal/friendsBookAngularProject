/// <reference types="cypress" />

import { MainPage } from "../../pageObjects/factory/mainPage";

let mainPage = new MainPage();

describe('New user registration', () => {

    before(() => {
        cy.visit(Cypress.env('registration'));
    });

    describe('Invalid or empty data in required fields', () => {

        it('Should display warning messages on touching required fields and leaving them empty', () => {

            cy.get('[required]').as('allRequiredFields');

            cy.get('@allRequiredFields').then(requiredFields => {
                for (let field of requiredFields) {
                    field.focus();
                    field.blur();
                    const fieldDataCyAttr = field.getAttribute('data-cy');
                    if (fieldDataCyAttr && fieldDataCyAttr !== 'dob') {
                        mainPage.sharedForm.getErrorElement(`${"parent" + fieldDataCyAttr}`)
                            .should('exist')
                            .should('be.visible');
                    }
                    else if (fieldDataCyAttr === 'dob') {
                        mainPage.sharedForm.getDateOfBirthError()
                            .should('exist')
                            .should('be.visible');
                    }
                }
            });

        });

        it('Should display warning messages on invalid email id', () => {

            mainPage.sharedForm.getEmailIdInputField().type('abc').blur();
            mainPage.sharedForm.getEmailIdError().should('exist').should('be.visible');

        });

        it('Should display warning messages on invalid password', () => {

            mainPage.sharedForm.getPasswordInputField().type('abc').blur();
            mainPage.sharedForm.getPasswordError().should('exist').should('be.visible');

        });

        it('Should display warning messages on invalid confirm password', () => {


            mainPage.sharedForm.getConfirmPasswordInputField().type('abc').blur();
            mainPage.sharedForm.getConfirmPasswordError().should('exist').should('be.visible');

        });

        it('Should have form submit button disabled', () => {
            mainPage.sharedForm.getSubmitButton().should('be.disabled');
        });
    });

    describe('Valid data in fields', () => {

        it('Should not display warning messages for required fields and have sign up button activated', () => {

            cy.reload();
            cy.fixture('../fixtures/user.json').as('user');

            cy.get('@user').then((userData) => {

                mainPage.sharedForm.getFirstNameInputField().as('firstNameField')
                    .type(userData.newUserData.firstName, { force: true }).blur();

                mainPage.sharedForm.getLastNameInputField().as('lastNameField')
                .type(userData.newUserData.lastName, { force: true })
                .blur();
                
                mainPage.sharedForm.getDateOfBirthInputField().as('dobField')
                .type(userData.newUserData.dob, { force: true })
                .blur();
                
                mainPage.sharedForm.getEmailIdInputField().as('emailIdField')
                .type(userData.newUserData.emailId)
                .blur();
                
                mainPage.sharedForm.getPasswordInputField().as('passwordField')
                .type(userData.newUserData.testPassword)
                .blur();
                
                mainPage.sharedForm.getConfirmPasswordInputField().as('confirmPasswordField')
                .type(userData.newUserData.testPassword)
                .blur();

            });

            cy.log('No Error Messages for correct data entered in required fields');
            mainPage.sharedForm.getEmailIdError().should('not.exist');

        });

        describe('On form submit', () => {

            it('Should create the user successfully and navigate the user to login page', () => {

                cy.intercept({
                    method: "POST",
                    url: "/users/register"
                }).as('registeredUser');


                mainPage.sharedForm.getSubmitButton()
                    .should('not.be.disabled')
                    .click()
                    .then(() => {
                        cy.wait('@registeredUser')
                            .its('response.body.message')
                            .should('eq', 'User registered successfully')
                    })
                    .then(() => {
                        cy.location('pathname')
                            .should('eq', '/login')
                    });
            });
        });
    });
})