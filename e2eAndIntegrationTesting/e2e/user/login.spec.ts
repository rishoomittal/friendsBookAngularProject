/// <reference types="cypress" />

import { MainPage } from "../../pageObjects/factory/mainPage";

let mainPage = new MainPage();

describe('Login User', () => {

    before(() => {
        cy.visit(Cypress.env('login'));
    });

    xdescribe('Invalid data in required fields', () => {

        it('Should for empty fields have form submit button disabled', () => {
            mainPage.sharedForm.getSubmitButton().should('be.disabled');
        });

        //To-do
        xit('Should show validation messages', () => {
            
        });
    });

    xdescribe('Valid data in fields', () => {

        it('Should login the user successfully', () => {
            cy.fixture('../fixtures/user.json').as('user');

            cy.get('@user').then((userData) => {

                mainPage.sharedForm.getEmailIdInputField().as('emailIdField')
                .type(userData.existingUser.emailId)
                .blur();
                
                mainPage.sharedForm.getPasswordInputField().as('passwordField')
                .type(userData.existingUser.password)
                .blur();

                mainPage.sharedForm.getSubmitButton()
                    .should('not.be.disabled')
                    .click().then(() => {
                        cy.location('pathname')
                            .should('eq', '/home');
                });
            });
        });
    });

    after(() => {
        localStorage.clear();
    })
})