/// <reference types="cypress" />

describe('All users in system list', () => {
    it('Should be visible only if user is admin', () => {

    });

    it('Should display all users in the system', () => {

    });

    it('Should show users first name, last name and email id', () => {

    });

    it('Should show users is active & blocked status', () => {

    });

    it('Should have snackbar displayed when user is activated or blocked', () => {

    });

    it('Should have snackbar displayed when user is made or removed from admin', () => {

    });    
});