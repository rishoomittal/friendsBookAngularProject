/// <reference types="cypress" />

import { postByUserIdUrl } from '../../src/app/constants/urlConstants'

describe('Posts by a user', () => {

    before(() => {
        cy.fixture('../fixtures/user.json').as('user');
        cy.get('@user').then((userData) => {
            cy.login(userData.emailId, userData.password);
        });
    });

    it('Should get all posts by user', () => {

        const token = localStorage.getItem('token')
        cy.log(token);
        const authorization = `bearer ${token}`;
        cy.request({
            method: "POST",
            url: postByUserIdUrl,
            headers: { authorization },
            body: {
                id: localStorage.getItem('id'),
            }
        }).then((response) => {
            expect(response).property('status').to.equal(200);
            expect(response.body.length).to.be.greaterThan(0);
        });
    });
});