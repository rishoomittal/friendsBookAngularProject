/// <reference types="cypress" />

describe('Login User', () => {

    before(() => {
        const interval = setInterval(() => {
            console.log("I am set interval");
        },500);

        cy.visit(Cypress.env('login'));
        console.log(Cypress.config().baseUrl);
        cy.log("I am cypress log");
        
        setTimeout(() => {
            console.log('I am a set timeout');
            clearInterval(interval);
        },2000);

        function abc() {
            return new Promise((resolve,reject) => {
                console.log('I am console log inside a Promise');
                resolve("I am a resolved promise");
                reject("The promise is rejected");
            });
        } 
        abc().then((resolve) => {
            console.log("Lets resolve this promise: " + resolve);
        });

        console.log("I am an independent console log statement");
    });
});