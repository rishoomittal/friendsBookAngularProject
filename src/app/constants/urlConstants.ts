export const serverBaseUrl = "http://nodejs-fb-app.herokuapp.com/";

//User related urls
export const allUsersUrl = serverBaseUrl + "users/";
export const authenticateUserUrl = allUsersUrl + "authenticate";

//posts related urls
export const allPostsUrl = serverBaseUrl + "posts/";
export const createPostUrl = allPostsUrl + "createpost";
export const postByUserIdUrl = allPostsUrl + "findpostbyuserid";
export const updateManyPostsUrl = allPostsUrl + "updatemanyposts";

//Friends related urls
export const allFriendRequestsUrl = serverBaseUrl + "friends/";
export const createFriendRequestUrl = allFriendRequestsUrl + "createrequest";

//File upload urls
export const allFilesUrl = serverBaseUrl + "files/";
export const uploadFileUrl = allFilesUrl + "uploadfile";


