import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListPageComponent } from './components/users-list-page/users-list-page.component';
import { AdminRoutingModule } from './admin-routing.module';
import { UserCardComponent } from './components/user-card/user-card.component';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../shared/shared.module';
import { AdminActionButtonsComponent } from './components/admin-action-buttons/admin-action-buttons.component';
import { MatButtonModule } from '@angular/material/button';
import { AuthInterceptor } from '../../interceptors/authentication.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserModule } from '../user/user.module';
import { SeeAllPostsComponent } from './components/see-all-posts/see-all-posts.component';
import { AdminUtilitiesService } from 'src/app/services/adminUtilities/admin-utilities.service';
import {MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';

@NgModule({
  declarations: [UsersListPageComponent, UserCardComponent, AdminActionButtonsComponent, SeeAllPostsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    BrowserModule,
    SharedModule,
    UserModule,
    MatButtonModule,
    MatSnackBarModule
  ],
  providers: [
    AdminUtilitiesService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}}
  ]
})
export class AdminModule { }
