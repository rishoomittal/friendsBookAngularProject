import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminActionButtonsComponent } from './admin-action-buttons.component';
import { AdminUtilitiesService } from 'src/app/services/adminUtilities/admin-utilities.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { noop } from 'rxjs';

describe('AdminActionButtonsComponent', () => {
  let component: AdminActionButtonsComponent;
  let fixture: ComponentFixture<AdminActionButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminActionButtonsComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule],
      providers: [{ provide: AdminUtilitiesService },
      {
        provide: AuthenticatedUserService, useValue: {
          isLoggedIn: noop,
          getLoggedInUser: () => {
            return 123;
          }
        }
      },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminActionButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
