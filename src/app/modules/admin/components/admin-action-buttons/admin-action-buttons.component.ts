import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { IPostUser } from 'src/app/modules/user/interfaces/user';
import { AdminUtilitiesService } from 'src/app/services/adminUtilities/admin-utilities.service';
import { SeeAllPostsComponent } from '../see-all-posts/see-all-posts.component';

@Component({
  selector: 'app-admin-action-buttons',
  templateUrl: './admin-action-buttons.component.html',
  styleUrls: ['./admin-action-buttons.component.sass']
})
export class AdminActionButtonsComponent implements OnInit {

  @Input()
  userId;

  @Input()
  userName;

  @Input()
  allUsersInSystem: IPostUser[];

  blockOrActivate: string = "";
  blockUserButton = "Block User";
  activateUserButton = "Activate User";

  userIsAdminOrNot: string = "";
  userIsAdmin = "Remove Admin Rights";
  userIsNotAdmin = "Make User Admin";

  seeAllPosts = "View Posts By User";

  constructor(private adminService: AdminUtilitiesService, public dialog: MatDialog, private _snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    for (let user of this.allUsersInSystem) {
      if (user._id == this.userId) {
        user.isActive ? this.blockOrActivate = this.blockUserButton : this.blockOrActivate = this.activateUserButton;
        user.isAdmin ? this.userIsAdminOrNot = this.userIsAdmin : this.userIsAdminOrNot = this.userIsNotAdmin;
      }
    }
  }

  blockOrActivateUser(blockOrActivate: string) {
    if (this.userId) {
      let activateUser: boolean = true;
      blockOrActivate == this.blockUserButton ? activateUser = false : false;
      this.processAdminActionRequest((this.adminService.blockOrActivateUser(this.userId, activateUser)),'blockOrActivate',activateUser);
  }
}

  makeUserAdmin(adminOrNot: string) {
    if (this.userId) {
      let makeUserAdminValue: boolean = true;
      adminOrNot == this.userIsAdmin ? makeUserAdminValue = false : false;
      this.processAdminActionRequest(this.adminService.makeUserAdmin(this.userId, makeUserAdminValue),'makeUserAdmin',makeUserAdminValue);
    }
  }

  seeAllPostsOfUser() {
    let dialogRef = this.dialog.open(SeeAllPostsComponent, {
      panelClass: 'custom-dialog',
      data: { userId: this.userId, username: this.userName }
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed'+ result.value);
    });
  }

  private processAdminActionRequest(request, switchString: string, booleanValue: boolean) {
    request.subscribe((response: { status: string; }) => {
      if (response.status == "200") {
        this.adminService.fetchAllUsersInSystemAsAdmin().subscribe(
          allUsers => this.allUsersInSystem = allUsers
          );
          switch(switchString) {
            case 'blockOrActivate': {
              booleanValue === false ? this.blockOrActivate = this.activateUserButton : this.blockOrActivate = this.blockUserButton;
              booleanValue === false ? this.showSnackBar("The user has been blocked") : this.showSnackBar("The user has been activated");
              break;
            }
            case 'makeUserAdmin': {
              booleanValue === false ? this.userIsAdminOrNot = this.userIsNotAdmin : this.userIsAdminOrNot = this.userIsAdmin;
              booleanValue === false ? this.showSnackBar("The user is removed as an admin") : this.showSnackBar("The user is now an admin");
              break;
            }
          }
      }
    });
  }

  private showSnackBar(message: string) {
    const durationInSeconds = 5;
    this._snackBar.open(message, '', {
      horizontalPosition: 'end',
      verticalPosition: 'bottom',
      duration: durationInSeconds * 1000,
    });
  }
}
