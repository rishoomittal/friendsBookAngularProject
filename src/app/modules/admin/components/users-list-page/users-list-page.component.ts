import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPostUser } from 'src/app/modules/user/interfaces/user';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.sass']
})
export class UsersListPageComponent implements OnInit {

  allUsersInSystem: IPostUser[] = [];

  constructor(private activatedRoute: ActivatedRoute) {

  }
  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      (data: IPostUser[]) => {
        this.allUsersInSystem = data["allUsers"];
      });
  }

}
