import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-see-all-posts',
  templateUrl: './see-all-posts.component.html',
  styleUrls: ['./see-all-posts.component.sass']
})
export class SeeAllPostsComponent {

  userId: string;
  
  nameOfUser: string;
  constructor(
    public dialogRef: MatDialogRef<SeeAllPostsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) {

    this.userId = data["userId"];
    this.nameOfUser = data["username"];
  }

}
