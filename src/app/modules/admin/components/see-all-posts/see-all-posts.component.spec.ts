import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/modules/root/app.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { AdminModule } from '../../admin.module';

import { SeeAllPostsComponent } from './see-all-posts.component';

describe('SeeAllPostsComponent', () => {
  let component: SeeAllPostsComponent;
  let fixture: ComponentFixture<SeeAllPostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SeeAllPostsComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule, AppModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeAllPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
