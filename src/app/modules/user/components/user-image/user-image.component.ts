import { Component, OnInit } from '@angular/core';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';

@Component({
  selector: 'app-user-image',
  templateUrl: './user-image.component.html',
  styleUrls: ['./user-image.component.sass']
})
export class UserImageComponent implements OnInit {

  photoId: string;
  userPhotoBlob: Blob;
  retrieveUrl: string | ArrayBuffer;

  constructor(private userAuthenticationService: AuthenticatedUserService) {
    this.photoId = this.userAuthenticationService.getPhotoIdFromStorage();
  }

  ngOnInit(): void {
    const userId = this.userAuthenticationService.getLoggedInUser()._id;

    this.userAuthenticationService.getUserById(userId).subscribe(
      (data) => {
        this.photoId = data["photoId"];
        this.userAuthenticationService.setUserPhotoIdInLocalStorage(this.photoId);

        if (this.photoId) {
          const photoBlob = this.userAuthenticationService.getUserImage(this.photoId);
          photoBlob.subscribe((dataBlob) => {
            this.userPhotoBlob = dataBlob;
            this.userPhotoBlob.text().then(blobText => this.userAuthenticationService.setUserPhotoBlobInLocalStorage(blobText));
            const reader = new FileReader();
            reader.readAsDataURL(dataBlob);
            reader.onload = _event => {
              this.retrieveUrl = reader.result;
            };
          })
        }
      });
  }

}
