import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { UserPostsService } from 'src/app/services/userPosts/user-posts.service';
import { IPostUser, IUserLoggedIn } from '../../interfaces/user';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.sass']
})
export class HomePageComponent implements OnInit {

  userId: string;
  newPostCreationStatus: boolean = false;

  constructor(private userService: AuthenticatedUserService, private renderer: Renderer2) {
    this.userId = this.userService.getLoggedInUser()._id;
  }

  ngOnInit(): void {
    this.renderer.removeClass(document.body, 'animation_bg');
  }

}
