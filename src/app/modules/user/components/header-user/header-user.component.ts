import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';

@Component({
  selector: 'app-header-user',
  templateUrl: './header-user.component.html',
  styleUrls: ['./header-user.component.sass']
})
export class HeaderUserComponent implements OnInit, OnDestroy {

  isAdmin = false;
  constructor(private userAuthenticationService: AuthenticatedUserService, private router: Router) {
  }

  ngOnInit(): void {
    this.isAdmin = this.userAuthenticationService.getLoggedInUser().isAdmin;
  }

  logout() {
    location.reload();
    this.userAuthenticationService.logout();
  }

  ngOnDestroy(): void {
    this.isAdmin = false;
    this.userAuthenticationService.logout();
  }

}
