import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { noop } from 'rxjs';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { UserPostsService } from 'src/app/services/userPosts/user-posts.service';
import { IPostByAUser } from '../../interfaces/user';

import { TotalPostsLabelComponent } from './total-posts-label.component';

describe('UserPostsComponent', () => {
  let component: TotalPostsLabelComponent;
  let fixture: ComponentFixture<TotalPostsLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TotalPostsLabelComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        {
          provide: AuthenticatedUserService, useValue: {
            isLoggedIn: noop,
            getLoggedInUser: () => {
              return { _id: 123 };
            }
          }
        },
        {
          provide: UserPostsService, useValue: {
            getAllPostsByAUser: () => {
              return new Promise((resolve) => {
                const allPosts: IPostByAUser[] = [];
                allPosts.push({_id : "123"});
                allPosts.push({_id : "124"});
                allPosts.push({_id : "125"});
                allPosts.push({_id : "126"});
                resolve(allPosts);
              });
            }
          }
        }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalPostsLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
