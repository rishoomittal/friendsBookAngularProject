import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { UserPostsService } from 'src/app/services/userPosts/user-posts.service';

@Component({
  selector: 'app-total-posts-label',
  templateUrl: './total-posts-label.component.html',
  styleUrls: ['./total-posts-label.component.sass']
})
export class TotalPostsLabelComponent implements OnChanges {

  userId: string;
  totalPosts: number = null;

  @Input()
  newPostCreationStatus = false;

  constructor(private postService: UserPostsService, private userAuthService: AuthenticatedUserService) {
    this.userId = this.userAuthService.getLoggedInUser()._id;
    this.postService.getAllPostsByAUser(this.userId).subscribe(posts => this.totalPosts = posts.length);
  }

  ngOnChanges(): void {
    if (this.newPostCreationStatus) {
      this.postService.getAllPostsByAUser(this.userId).subscribe(posts => this.totalPosts = posts.length);
      this.newPostCreationStatus
    }
  }

}
