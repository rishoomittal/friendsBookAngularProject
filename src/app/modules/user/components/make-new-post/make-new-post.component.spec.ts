import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { noop } from 'rxjs';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { UserPostsService } from 'src/app/services/userPosts/user-posts.service';

import { MakeNewPostComponent } from './make-new-post.component';

describe('MakeNewPostComponent', () => {
  let component: MakeNewPostComponent;
  let fixture: ComponentFixture<MakeNewPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MakeNewPostComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        {
          provide: AuthenticatedUserService, useValue: {
            isLoggedIn: noop,
            getLoggedInUser: () => {
              return 123;
            }
          }
        },
        { provide: UserPostsService }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeNewPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
