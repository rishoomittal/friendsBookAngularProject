import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { UserPostsService } from 'src/app/services/userPosts/user-posts.service';
import { IPostByAUser } from '../../interfaces/user';

@Component({
  selector: 'app-make-new-post',
  templateUrl: './make-new-post.component.html',
  styleUrls: ['./make-new-post.component.sass']
})
export class MakeNewPostComponent {

  newPost: string;

  @Output()
  newPostCreationStatus = new EventEmitter<boolean>();


  constructor(private postService: UserPostsService, private userService: AuthenticatedUserService) { }

  createNewPost() {

    const newPostByuser: IPostByAUser = {};
    newPostByuser.post = this.newPost;
    newPostByuser.createdDate = new Date();
    newPostByuser.userId = this.userService.getLoggedInUser()._id;
    newPostByuser.isActive = true;
    newPostByuser.isAdmin = this.userService.getLoggedInUser().isAdmin;
    newPostByuser.profession = "user";
    newPostByuser.userName = this.userService.getLoggedInUser().email;

    const newPostResponse = this.postService.createNewPost(newPostByuser);
    newPostResponse.subscribe(post => {
      if (post.message === "Post created successfully") {
        this.newPostCreationStatus.emit(true);
        this.newPost = "";
      }
    });
  }
}
