import { Component, Input, OnInit } from '@angular/core';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { FriendRequestService } from 'src/app/services/friendRequests/friend-request.service';

@Component({
  selector: 'app-friend-request',
  templateUrl: './friend-request.component.html',
  styleUrls: ['./friend-request.component.sass']
})
export class FriendRequestComponent {

  @Input()
  userIdOfNetwork;

  @Input()
  friendRequestIdsOfUnavailableUsers;

  @Input()
  requestStatusTitle;

  userId;

  constructor(private friendService: FriendRequestService, private userAuthService: AuthenticatedUserService) {
    this.userId = this.userAuthService.getLoggedInUser()._id;
  }

  createOrWithdrawRequest(requestStatusTitle) {
    requestStatusTitle === "Send Request" ? this.createRequest() : false;
    requestStatusTitle === "Unfriend User" ? this.withdrawRequest() : false;
    //requestStatusTitle === "Request Sent" ? this.withdrawRequest() : false;
  }

  createRequest() {
    const loggedInUserId = this.userAuthService.getLoggedInUser()._id;
    const requestStatus = "Request Pending";

    const receivedObject = this.friendService.sendFriendRequest(this.userIdOfNetwork, loggedInUserId, requestStatus);

    receivedObject.subscribe(response => {
      if (response.message === "Friend added successfully") {
        this.requestStatusTitle = "Request Sent";
      }
    });
  }

  withdrawRequest() {
    const loggedInUserId = this.userAuthService.getLoggedInUser()._id;
    const requestStatus = "User Unfriended";
    const friendRequestID = this.friendRequestIdsOfUnavailableUsers;
    const receivedObject = this.friendService.updateFriendRequest(friendRequestID, this.userIdOfNetwork, loggedInUserId, requestStatus);

    receivedObject.subscribe((response: { message: string; }) => {
      if (response["status"] == "200") {
        this.requestStatusTitle = "Removed From Friends";

      }
    });
  }

}
