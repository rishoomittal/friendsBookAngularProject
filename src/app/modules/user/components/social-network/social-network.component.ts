import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { FriendRequestService } from 'src/app/services/friendRequests/friend-request.service';
import { IFriendRequest } from '../../interfaces/frinedRequest';
import { IPostUser } from '../../interfaces/user';

@Component({
  selector: 'app-social-network',
  templateUrl: './social-network.component.html',
  styleUrls: ['./social-network.component.sass']
})
export class SocialNetworkComponent implements OnInit, OnDestroy {

  allFriendIds: string[] = [];
  allFriendRequests: IFriendRequest[] = [];
  availableNetwork: IPostUser[] = [];

  unAvailableNetwork: IPostUser[] = [];
  friendRequestIdsOfUnavailableUsers: string[] = [];

  userId: string;
  availableNetowrkButtonTitle = "Send Request";
  unAvailableNetowrkButtonTitle = "Unfriend User";
  availableRetrievedPhoto: string[] = [];
  unavailableRetrievedPhoto: string[] = [];


  constructor(private activatedRoute: ActivatedRoute, private friendService: FriendRequestService,
    private userAuthService: AuthenticatedUserService) {
    this.userId = this.userAuthService.getLoggedInUser()._id;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      (data: IFriendRequest[]) => {
        this.friendService.allFriendRequests = data["data"];
        this.allFriendRequests = this.friendService.getAllFriendRequestsAssociatedToUser(this.userId);
        this.allFriendIds = this.friendService.getAllFriendIdAssociatedToUser(this.userId);
        this.fillAvailableAndUnavailableNetwork();
      });
  }

  fillAvailableAndUnavailableNetwork() {
    this.userAuthService.fetchAllUsersInSystem().subscribe(allUsers => {
      if (this.allFriendIds.length > 0) {
        for (let user of allUsers) {
          if (this.allFriendIds.indexOf(user._id) < 0) {
            this.availableNetwork.push(user);
            this.setUserImageForTheNetwork(user);
          }
          else {
            for (let friendRequest of this.allFriendRequests) {
              if (friendRequest.status.toString() != "User Unfriended" && friendRequest.friendId == user._id) {
                this.unAvailableNetwork.push(user);
                this.friendRequestIdsOfUnavailableUsers.push(friendRequest._id);
                this.setUserImageForTheNetwork(user, false);
              }
            }
          }
        }
      }
      else {
        this.availableNetwork.push(...allUsers);
      }
    });
  }

  setUserImageForTheNetwork(user: IPostUser, isAvailableNetwork: boolean = true) {
    let retrieveUrl: string | ArrayBuffer;

    const photoBlob = this.userAuthService.getUserImage(user.photoId);
    photoBlob.subscribe((dataBlob) => {
      const reader = new FileReader();
      reader.readAsDataURL(dataBlob);
      reader.onload = _event => {
        retrieveUrl = reader.result;
        if (isAvailableNetwork) {
          this.availableRetrievedPhoto[user.photoId] = retrieveUrl;
        }
        else {
          this.unavailableRetrievedPhoto[user.photoId] = retrieveUrl;
        }
      };
    });
  }

  ngOnDestroy(): void {
    this.allFriendIds = null;
    this.allFriendRequests = null;
  }

}
