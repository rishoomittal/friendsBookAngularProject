import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { basicTitlesForForm, getTriggerValuePair, IShowFormFields } from 'src/app/modules/shared/components/shared-form/showFormAttributes';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { IPostUser, IUser } from 'src/app/modules/user/interfaces/user';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.sass']
})
export class ProfileSettingsComponent implements OnInit {

  formType: string = "profileSettings";
  showFields: IShowFormFields;
  basicTitles = basicTitlesForForm;
  submitButtonTitle = "Update";
  fillFormFields = true;

  filledForm: FormGroup;
  imageId: string;

  constructor(private userAuthenticationService: AuthenticatedUserService,
    private router: Router) {

    if (this.userAuthenticationService.isLoggedIn() == false) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit(): void {

    this.showFields = {
      firstName: getTriggerValuePair(this.basicTitles.firstName, true),
      lastName: getTriggerValuePair(this.basicTitles.lastName, true),
      emailId: getTriggerValuePair(this.basicTitles.emailId, true),
      dob: getTriggerValuePair(this.basicTitles.dob, true),
      gender: getTriggerValuePair(this.basicTitles.gender, true),
      phone: getTriggerValuePair(this.basicTitles.phone, true),
      city: getTriggerValuePair(this.basicTitles.city, true),
      country: getTriggerValuePair(this.basicTitles.country, true),
      pincode: getTriggerValuePair(this.basicTitles.pincode, true),
      photoId: getTriggerValuePair(this.basicTitles.photoId, true),
      submitButton: getTriggerValuePair(this.submitButtonTitle, true)
    };

  }

  asignFilledForm(filledForm: FormGroup) {
    this.filledForm = filledForm;
    if (this.filledForm) {
      this.updateProfile();
    }
  }

  async updateProfile() {
    let user: IPostUser = {
      firstName: "",
      lastName: "",
      dob: "",
      phone: "",
      city: "",
      state: "",
      country: "",
      pincode: "",
      gender: "",
      photoId: ""
    };

    let userKeys = Object.keys(user);
    for (const key of Object.keys(this.showFields)) {
      if (userKeys.indexOf(key) > -1) {
        console.log(key);
        if (key == "photoId") {
          this.userAuthenticationService.setUserPhotoIdInLocalStorage(this.imageId);
          user[key] = this.imageId;
        }
        else
          user[key] = this.filledForm.get(key).value;
      }
    }

    if (await (await (this.userAuthenticationService.updateUserDetails(user))).toPromise()) {
      location.reload();
    }

  }

  asignImage(file: File) {

    let selectedFile: ImageSnippet;
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {

      selectedFile = new ImageSnippet(event.target.result, file);

      this.userAuthenticationService.uploadImage(selectedFile.file).subscribe(
        (data) => {
          this.imageId = data["uploadId"];
          console.log(this.imageId);
        },
        (err) => {
          console.log("Unable to update Image", err);
        })
    });

    reader.readAsDataURL(file);
  }
}

class ImageSnippet {
  constructor(public src: string, public file: File) { }
}