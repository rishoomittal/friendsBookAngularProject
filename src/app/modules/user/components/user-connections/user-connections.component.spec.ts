import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { noop } from 'rxjs';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { FriendRequestService } from 'src/app/services/friendRequests/friend-request.service';

import { UserConnectionsComponent } from './user-connections.component';

describe('UserConnectionsComponent', () => {
  let component: UserConnectionsComponent;
  let fixture: ComponentFixture<UserConnectionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserConnectionsComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        {
          provide: AuthenticatedUserService, useValue: {
            isLoggedIn: noop,
            getLoggedInUser: () => {
              return 123;
            }
          }
        },
        { provide: FriendRequestService }
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserConnectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
