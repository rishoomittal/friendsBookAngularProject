import { Component, OnInit } from '@angular/core';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { FriendRequestService } from 'src/app/services/friendRequests/friend-request.service';

@Component({
  selector: 'app-user-connections',
  templateUrl: './user-connections.component.html',
  styleUrls: ['./user-connections.component.sass']
})
export class UserConnectionsComponent {

  userId: string;
  totalConnections: number = 0;

  constructor(private friendService: FriendRequestService, private userAuthService: AuthenticatedUserService) {
    this.userId = this.userAuthService.getLoggedInUser()._id;
    //this.totalConnections = this.friendService.getAllFriendIdAssociatedToUser(this.userId).length;
  }

}
