import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPostsByUserComponent } from './all-posts-by-user.component';

describe('AllPostsByUserComponent', () => {
  let component: AllPostsByUserComponent;
  let fixture: ComponentFixture<AllPostsByUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AllPostsByUserComponent],
      imports: [HttpClientTestingModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPostsByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
