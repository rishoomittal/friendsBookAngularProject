import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { UserPostsService } from 'src/app/services/userPosts/user-posts.service';
import { IPostByAUser } from '../../interfaces/user';

@Component({
  selector: 'app-all-posts-by-user',
  templateUrl: './all-posts-by-user.component.html',
  styleUrls: ['./all-posts-by-user.component.sass']
})
export class AllPostsByUserComponent implements OnInit, OnChanges {

  @Input()
  userId: string;

  allPosts: IPostByAUser[] = [];

  @Input()
  newPostCreationStatus: boolean;

  @Output()
  changeInNewPostCreationStatus = new EventEmitter<boolean>();

  constructor(private postService: UserPostsService, private userService: AuthenticatedUserService) {
    
  }

  ngOnInit(): void {
    this.showAllPostByUser().subscribe(
      allPosts => this.allPosts = allPosts
      );
  }

  ngOnChanges(): void {
    if (this.newPostCreationStatus) {
      this.showAllPostByUser().subscribe(allPosts => this.allPosts = allPosts);
      this.newPostCreationStatus = false;
      this.changeInNewPostCreationStatus.emit(this.newPostCreationStatus);
    }
  }

  showAllPostByUser(): Observable<IPostByAUser[]> {
    return this.postService.getAllPostsByAUser(this.userId);
  }

}
