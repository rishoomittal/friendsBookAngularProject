import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { FriendRequestService } from 'src/app/services/friendRequests/friend-request.service';
import { IFriendRequest } from '../../interfaces/frinedRequest';
import { IPostUser } from '../../interfaces/user';

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.sass']
})
export class FriendListComponent implements OnInit {

  userId: string;
  allFriendRequestsOfLoggedInUser: IFriendRequest[] = [];
  allFriends: IPostUser[] = [];
  dataSource = this.allFriends;
  displayedColumns: string[] = ['firstName', 'lastName', 'email'];

  constructor(private activatedRoute: ActivatedRoute, private friendService: FriendRequestService, private userAuthService: AuthenticatedUserService) {
    this.userId = this.userAuthService.getLoggedInUser()._id;
  }

  ngOnInit(): void {

    this.activatedRoute.data.subscribe(

      (data: IFriendRequest[]) => {
        this.friendService.allFriendRequests = data["data"];

        if (this.userId && this.friendService.allFriendRequests) {
          this.allFriendRequestsOfLoggedInUser = this.friendService.getAllFriendRequestsAssociatedToUser(this.userId);
        }

        for (let friendRequest of this.allFriendRequestsOfLoggedInUser) {

          if (friendRequest.status.toString() == "Request Accepted") {

            this.userAuthService.fetchAllUsersInSystem().subscribe(allUsersInSystem => {
              for (let userInSystem of allUsersInSystem) {
                if (userInSystem._id == friendRequest.friendId) {
                  this.allFriends.push(userInSystem);
                }
              }

            });
          }
        }
      });
  }
}
