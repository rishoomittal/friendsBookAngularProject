export interface IFriendRequest {
    _id?: string,
    userId?: string,
    friendId?: string,
    status?: string,
    createdDate?: string,
    __v?: string,
    id?: string
}