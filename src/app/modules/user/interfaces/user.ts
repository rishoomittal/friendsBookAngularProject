export interface IUser {
    firstName?: string,
    lastName?: string,
    emailId?: string,
    password?: string,
    remark?: string,
    dob?: string,
    phone?: string,
    city?: string,
    state?: string,
    country?: string,
    pincode?: string,
    gender?: string,
    photoId?: string,
    isAdmin?: boolean,
    createdDate?: string
}

export interface IPostUser {
    createdDate?: string,
    dob?: string,
    email?: string,
    firstName?: string,
    gender?: string,
    isActive?: boolean,
    isAdmin?: boolean,
    lastName?: string,
    photoId?: string,
    phone?: string,
    city?: string,
    state?: string,
    country?: string,
    pincode?: string
    token?: string,
    _id?: string
}

export interface IUserLoggedIn {
    createdDate?: string,
    dob?: string,
    email?: string,
    firstName?: string,
    isActive?: boolean,
    isAdmin?: boolean,
    lastName?: string,
    photoId?: string,
    phone?: string,
    city?: string,
    state?: string,
    country?: string,
    pincode?: string,
    gender?: string
    _id?: string
}

export interface IPostByAUser {
    _id?: string,
    post?: string,
    createdDate?: Date,
    userId?: string,
    userName?: string,
    userPhotoId?: string,
    postImageId?: string,
    isAdmin?: boolean,
    isActive?: boolean,
    profession?: string
}

