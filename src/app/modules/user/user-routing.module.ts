import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ProfileSettingsComponent } from './components/settings/profile-settings/profile-settings.component';
import { FriendListComponent } from './components/friend-list/friend-list.component';
import { SocialNetworkComponent } from './components/social-network/social-network.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { UserAuthenticationGuard } from '../../guards/user-authentication.guard';
import { UsersListPageComponent } from '../admin/components/users-list-page/users-list-page.component';
import { AllFriendRequestResolver } from '../../resolvers/allFriendRequest.resolve';
import { UserListResolve } from '../../resolvers/userListResolve.resolve';
import { SettingsComponent } from './components/settings/settings.component';

export const userRoutes: Routes = [
    { path: 'home', component: HomePageComponent, canActivate: [UserAuthenticationGuard] },
    { path: 'settings', component: SettingsComponent, canActivate: [UserAuthenticationGuard] },
    {
        path: 'friends', component: FriendListComponent, canActivate: [UserAuthenticationGuard], resolve: {
            data: AllFriendRequestResolver
        }
    },
    {
        path: 'socialNetwork', component: SocialNetworkComponent, canActivate: [UserAuthenticationGuard], resolve: {
            data: AllFriendRequestResolver
        }
    },
    { path: 'notifications', component: NotificationsComponent, canActivate: [UserAuthenticationGuard] },
    {
        path: 'userListPage', component: UsersListPageComponent, canActivate: [UserAuthenticationGuard],
        resolve: { allUsers: UserListResolve }
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(userRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [AllFriendRequestResolver, UserListResolve]
})
export class UserRoutingModule { }