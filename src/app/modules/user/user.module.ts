import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './components/home-page/home-page.component';
import { SocialNetworkComponent } from './components/social-network/social-network.component';
import { FriendListComponent } from './components/friend-list/friend-list.component';
import { ProfileSettingsComponent } from './components/settings/profile-settings/profile-settings.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { UserRoutingModule } from './user-routing.module';
import { UserImageComponent } from './components/user-image/user-image.component';
import { UserConnectionsComponent } from './components/user-connections/user-connections.component';
import { TotalPostsLabelComponent } from './components/total-posts-label/total-posts-label.component';
import { FriendRequestComponent } from './components/friend-request/friend-request.component';
import { UserComponent } from './user.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MatInputModule } from '@angular/material/input';
import { AdminRoutingModule } from '../admin/admin-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../../interceptors/authentication.interceptor';
import { FriendProfileComponent } from './components/friend-profile/friend-profile.component';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { SettingsComponent } from './components/settings/settings.component';
import { AllPostsByUserComponent } from './components/all-posts-by-user/all-posts-by-user.component';
import { MakeNewPostComponent } from './components/make-new-post/make-new-post.component';
import { SortByDatePipe } from '../../pipes/sort-by-date.pipe';
import { ScrollingModule } from '@angular/cdk/scrolling';


@NgModule({
  declarations: [HomePageComponent, SocialNetworkComponent, FriendListComponent,
    ProfileSettingsComponent, NotificationsComponent,
    UserImageComponent, UserConnectionsComponent, FriendRequestComponent, UserComponent, SettingsComponent, FriendProfileComponent, TotalPostsLabelComponent, AllPostsByUserComponent, MakeNewPostComponent, SortByDatePipe],
  imports: [
    CommonModule,
    UserRoutingModule,
    RouterModule,
    SharedModule,
    MatInputModule,
    AdminRoutingModule,
    MatTableModule,
    MatTabsModule,
    ScrollingModule
  ],
  exports: [
    AllPostsByUserComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
})
export class UserModule { }
