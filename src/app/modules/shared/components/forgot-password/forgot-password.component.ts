import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticatedUserService } from '../../../../services/authentication/authenticated-user.service';
import { IShowFormFields, basicTitlesForForm, getTriggerValuePair } from '../shared-form/showFormAttributes';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.sass']
})
export class ForgotPasswordComponent implements OnInit {

  formType: string = "forgotPassword";
  showFields: IShowFormFields;
  basicTitles = basicTitlesForForm;
  formTitle = "Forgot Password";
  submitButtonTitle = "Send Password Reset link";

  constructor(private userAuthenticationService: AuthenticatedUserService, private router: Router) {

    if (this.userAuthenticationService.isLoggedIn()) {
      this.router.navigate(['/home']);
    }

  }

  ngOnInit(): void {

    this.showFields = {
      formTitle: getTriggerValuePair(this.formTitle, true),
      emailId: getTriggerValuePair(this.basicTitles.emailId, true),
      submitButton: getTriggerValuePair(this.submitButtonTitle, true),
    }
  }

  onSubmit() {

  }

}
