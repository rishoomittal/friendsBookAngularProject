import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IPostUser } from 'src/app/modules/user/interfaces/user';
import { AuthenticatedUserService } from '../../../../services/authentication/authenticated-user.service';
import { getTriggerValuePair, IShowFormFields, basicTitlesForForm } from '../shared-form/showFormAttributes';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  formType: string = "login";
  showFields: IShowFormFields;
  basicTitles = basicTitlesForForm;
  showSpinner = false;
  authenticationFailed = false;

  formTitle = "User Login";
  submitButtonTitle = "Sign In";
  filledForm: FormGroup;

  constructor(private userAuthenticationService: AuthenticatedUserService, private router: Router) {

    if (this.userAuthenticationService.isLoggedIn()) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit(): void {

    this.showFields = {
      formTitle: getTriggerValuePair(this.formTitle, true),
      emailId: getTriggerValuePair(this.basicTitles.emailId, true),
      password: getTriggerValuePair(this.basicTitles.password, true),
      submitButton: getTriggerValuePair(this.submitButtonTitle, true)
    };
  }

  loginUser(filledForm: FormGroup) {
    this.showSpinner = true;
    this.filledForm = filledForm;
    const email = this.filledForm.get('emailId').value;
    const passowrd = this.filledForm.get('password').value;

    const authenticatedUser = this.userAuthenticationService.authenticateUser(email, passowrd);
    
    authenticatedUser.subscribe(
      (user: IPostUser) => {
        this.userAuthenticationService.setLoggedInUserSession(user);
        this.showSpinner = false;
        this.router.navigate(['/home']);
        location.reload();
      },
      error => {
        this.showSpinner = false;
        this.authenticationFailed = true;
      }
    );
  }

}
