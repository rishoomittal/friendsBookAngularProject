import { RegistrationComponent } from './registration/registration.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { HeaderNewUserComponent } from './header-new-user/header-new-user.component';
import { SharedFormComponent } from './shared-form/shared-form.component';
import { ColorThemeComponent } from './color-theme/color-theme.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { HeaderUserComponent } from 'src/app/modules/user/components/header-user/header-user.component';
import { ErrorMessagesComponent } from './error-messages/error-messages.component';
import { PrivacySettingsComponent } from './privacy-settings/privacy-settings.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

export const components: any[] = [RegistrationComponent, ForgotPasswordComponent,
    HeaderComponent, FooterComponent, LoginComponent, HeaderNewUserComponent, SharedFormComponent,
    ColorThemeComponent, FeedbackComponent, HeaderUserComponent,
    ErrorMessagesComponent, PrivacySettingsComponent, ChangePasswordComponent];

export * from './registration/registration.component';
export * from './forgot-password/forgot-password.component';
export * from './shared-form/shared-form.component';
