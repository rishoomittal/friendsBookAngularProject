import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../footer/footer.component';
import { basicTitlesForForm, getTriggerValuePair, IShowFormFields } from '../shared-form/showFormAttributes';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.sass', '../../shared.component.sass']
})
export class FeedbackComponent {

  formType: string = "feedback";
  showFields: IShowFormFields;
  basicTitles = basicTitlesForForm;

  formTitle = "Feedback";

  validForm: boolean;

  constructor(public dialogRef: MatDialogRef<FeedbackComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    this.showFields = {
      firstName: getTriggerValuePair(this.basicTitles.firstName, true),
      emailId: getTriggerValuePair(this.basicTitles.emailId, true),
      remark: getTriggerValuePair(this.basicTitles.remark, true)
    };

  }

  updateValidForm(valid: boolean) {
    this.validForm = valid;
  }

  submit() {
    if (this.validForm) {
      console.log("Submitted form Feedback");
    }
  }

}