import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticatedUserService } from '../../../../services/authentication/authenticated-user.service';
import { basicTitlesForForm, getTriggerValuePair, IShowFormFields } from '../shared-form/showFormAttributes';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass']
})
export class ChangePasswordComponent implements OnInit {

  formType: string = "changePassword";
  showFields: IShowFormFields;
  basicTitles = basicTitlesForForm;

  submitButtonTitle = "Change Password";

  filledForm: FormGroup;
  passwordChangeStatus: boolean = false;
  passwordChangeMessage: string;

  constructor(private router: Router, private userService: AuthenticatedUserService) {

  }

  ngOnInit(): void {

    this.showFields = {
      password: getTriggerValuePair(this.basicTitles.password, true),
      confirmPassword: getTriggerValuePair(this.basicTitles.confirmPassword, true),
      submitButton: getTriggerValuePair(this.submitButtonTitle, true)
    };

  }

  asignFilledForm(filledForm: FormGroup) {
    this.filledForm = filledForm;
    if (this.filledForm) {
      this.updatePassword();
    }
  }

  updatePassword() {
    const password = this.filledForm.get('password').value;
    this.userService.updateUserPassword(password).subscribe(
      (data: HttpResponse<{}>) => {
        if (data.status == 200) {
          this.passwordChangeMessage = "Password successfully changed";
          this.passwordChangeStatus = true;
        }
        else {
          this.passwordChangeMessage = "An error occured while changing the password";
          this.passwordChangeStatus = true;
        }
      });
  }

}
