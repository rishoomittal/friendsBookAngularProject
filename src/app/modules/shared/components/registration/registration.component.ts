import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IUser } from 'src/app/modules/user/interfaces/user';
import { NewUserService } from '../../../../services/newUser/new-user.service';
import { AuthenticatedUserService } from '../../../../services/authentication/authenticated-user.service';
import { getTriggerValuePair, IShowFormFields, basicTitlesForForm } from '../shared-form/showFormAttributes';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

  formType: string = "registration";
  showFields: IShowFormFields;
  basicTitles = basicTitlesForForm;

  formTitle = "New User Registration";
  submitButtonTitle = "Sign Up";

  filledForm: FormGroup;

  constructor(private newUserService: NewUserService,
    private userAuthenticationService: AuthenticatedUserService,
    private router: Router) {

    if (this.userAuthenticationService.isLoggedIn()) {
      this.router.navigate(['/home']);
    }

  }

  ngOnInit(): void {

    this.showFields = {
      formTitle: getTriggerValuePair(this.formTitle, true),
      firstName: getTriggerValuePair(this.basicTitles.firstName, true),
      lastName: getTriggerValuePair(this.basicTitles.lastName, true),
      gender: getTriggerValuePair(this.basicTitles.gender, true),
      emailId: getTriggerValuePair(this.basicTitles.emailId, true),
      password: getTriggerValuePair(this.basicTitles.password, true),
      confirmPassword: getTriggerValuePair(this.basicTitles.confirmPassword, true),
      dob: getTriggerValuePair(this.basicTitles.dob, true),
      submitButton: getTriggerValuePair(this.submitButtonTitle, true)
    };

  }

  asignFilledForm(filledForm: FormGroup) {
    this.filledForm = filledForm;
    if (this.filledForm) {
      this.createNewUser();
    }
  }

  async createNewUser() {
    let user: IUser = {
      firstName: "",
      lastName: "",
      emailId: "",
      password: "",
      gender: "",
      dob: ""
    };
    let userKeys = Object.keys(user);
    for (const key of Object.keys(this.showFields)) {
      if (userKeys.indexOf(key) > -1) {
        user[key] = this.filledForm.get(key).value;

      }
    }
    this.newUserService.createUser(user).subscribe(response => {
      if (response.message === "User registered successfully") {
        this.router.navigate(['/login']);
      }
    });
  }

  public executeImportantAction(): void {
    //this.recaptchaV3Service.execute('importantAction').subscribe((token) => this.handleToken(token));
  }

}