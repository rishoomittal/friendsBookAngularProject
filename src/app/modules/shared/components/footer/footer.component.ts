import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FeedbackComponent } from '../feedback/feedback.component';
import { PrivacySettingsComponent } from '../privacy-settings/privacy-settings.component';

export interface DialogData {
  firstname: string;
  emailId: string;
  remark: string;
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})

export class FooterComponent {

  firstname: string;
  emailId: string;
  remark: string;
  privacySettingsContent: string;

  constructor(public dialog: MatDialog) { }

  openFeedbackDialog() {
    let dialogRef = this.dialog.open(FeedbackComponent, {
      panelClass: 'custom-dialog',
      data: { firstName: this.firstname, emailId: this.emailId, remark: this.remark }
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed'+ result.value);
    });
  }

  openPrivacySettingsDialog() {
    let dialogRef = this.dialog.open(PrivacySettingsComponent, {
      panelClass: 'custom-dialog',
      data: { privacySettingsContent: this.privacySettingsContent }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  submit() {
    console.log("Hello");
  }

}
