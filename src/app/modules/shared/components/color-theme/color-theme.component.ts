import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-color-theme',
  templateUrl: './color-theme.component.html',
  styleUrls: ['./color-theme.component.sass']
})
export class ColorThemeComponent {

  @Output()
  changed = new EventEmitter<boolean>();

  constructor() { }

}
