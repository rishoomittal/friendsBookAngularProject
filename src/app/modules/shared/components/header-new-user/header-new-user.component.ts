import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header-new-user',
  templateUrl: './header-new-user.component.html',
  styleUrls: ['./header-new-user.component.sass']
})
export class HeaderNewUserComponent {

  checked: boolean = false;

  constructor(private route: ActivatedRoute, private router: Router) {

  }

  gotoLoginPage() {
    this.router.navigate(['/login']);
  }
}
