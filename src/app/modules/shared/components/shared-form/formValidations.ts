import { Validators, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export const confirmPasswordValidationReactiveForm: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');
  if (password.value && confirmPassword.value) {
    return password.value === confirmPassword.value ? null : { noMatch: true };
  }
  return { noMatch: true };
};

export function emailValidation(formControl: AbstractControl) {
  formControl.setValidators([Validators.email]);
}

export function passwordValidation(formControl: AbstractControl) {
  const passwordValidationPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}';
  formControl.setValidators([Validators.pattern(passwordValidationPattern), Validators.minLength(8), Validators.maxLength(30)]);
}