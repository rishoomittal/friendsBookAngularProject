import { AfterViewChecked, AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { getFormGroup, IShowFormFields } from './showFormAttributes';
import { FormBuilder, FormGroup } from '@angular/forms';
import { confirmPasswordValidationReactiveForm, emailValidation, passwordValidation, } from "./formValidations";
import { AuthenticatedUserService } from '../../../../services/authentication/authenticated-user.service';


@Component({
  selector: 'app-shared-form',
  templateUrl: './shared-form.component.html',
  styleUrls: ['./shared-form.component.sass']
})
export class SharedFormComponent implements OnInit, AfterViewInit, AfterViewChecked {

  @Input()
  showFieldsAttr: IShowFormFields = {} as IShowFormFields;

  @Input()
  formType: string;

  @Output()
  sharedFormValid: EventEmitter<boolean> = new EventEmitter;

  @Output()
  filledForm: EventEmitter<FormGroup> = new EventEmitter;

  @Input()
  fillFormFields: boolean;

  @Output()
  image: EventEmitter<File> = new EventEmitter;

  sharedForm: FormGroup;
  valueCheck: boolean = false;

  constructor(private fb: FormBuilder, private userService: AuthenticatedUserService, private el: ElementRef) {
  }

  ngAfterViewChecked(): void {
    if (this.sharedForm && this.formType === 'feedback') {
      let flag = true;
      for (const controlName in this.sharedForm.controls) {
        if (this.sharedForm.get(controlName).invalid) {
          flag = false;
          this.valueCheck = false;
          break;
        }
      }
      if (flag && !this.valueCheck) {
        this.sharedFormValid.emit(flag);
        this.valueCheck = true;
      }
    }
  }


  ngOnInit(): void {
    this.sharedForm = this.fb.group(getFormGroup(this.showFieldsAttr));
    this.setValidatorsAsFormType();
    if (this.fillFormFields) {
      this.fillFormFieldExisitngValues();
    }
  }

  ngAfterViewInit() {

    //Added data-cy attribute for cypress
    let allFormFields = (<HTMLElement>this.el.nativeElement).querySelectorAll('.form-control');

    allFormFields.forEach(field => {
      field.setAttribute('data-cy', field.getAttribute('id'));
      field.parentElement.parentElement.setAttribute('data-cy', "parent" + field.getAttribute('id'));
    });
  }

  fillFormFieldExisitngValues() {

    const user = this.userService.getUserById(this.userService.getLoggedInUser()._id);
    user.subscribe((data) => {

      this.sharedForm.get('firstName').setValue(data.firstName);
      this.sharedForm.get('lastName').setValue(data.lastName);
      this.sharedForm.get('dob').setValue(new Date(data.dob).toDateString());
      this.sharedForm.get('emailId').setValue(data.email);
      this.sharedForm.get('phone').setValue(data.phone);
      this.sharedForm.get('city').setValue(data.city);
      this.sharedForm.get('country').setValue(data.country);
      this.sharedForm.get('pincode').setValue(data.pincode);
      this.sharedForm.get('gender').setValue(data.gender);
    });

    this.sharedForm.get('emailId').disable();
    this.sharedForm.get('dob').disable();
    this.sharedForm.get('gender').disable();
  }

  setValidatorsAsFormType() {
    switch (this.formType) {
      case 'registration': {
        if (this.showFieldsAttr.password.trigger === true && this.showFieldsAttr.confirmPassword.trigger === true) {
          this.sharedForm.validator = confirmPasswordValidationReactiveForm;
        }
        emailValidation(this.sharedForm.get('emailId'));
        passwordValidation(this.sharedForm.get('password'));
        passwordValidation(this.sharedForm.get('confirmPassword'));
        break;
      }
      case 'login': {
        emailValidation(this.sharedForm.get('emailId'));
        //passwordValidation(this.sharedForm.get('password'));
        break;
      }
      case 'forgotPassword': {
        emailValidation(this.sharedForm.get('emailId'));
        break;
      }
      case 'feedback': {
        emailValidation(this.sharedForm.get('emailId'));
        break;
      }
    }
  }

  onSubmit() {
    if (this.sharedForm.valid) {
      this.filledForm.emit(this.sharedForm);
    }
  }

  uploadImage(event) {
    console.log("I am in change event");
    const file = (event.target as HTMLInputElement).files[0];
    this.image.emit(file);
  }

}
