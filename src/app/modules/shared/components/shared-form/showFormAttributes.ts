import { Validators, FormGroup } from "@angular/forms";

export interface IShowFormFields {
    formTitle?: ITriggerValuePair,
    firstName?: ITriggerValuePair,
    lastName?: ITriggerValuePair,
    emailId?: ITriggerValuePair,
    password?: ITriggerValuePair,
    confirmPassword?: ITriggerValuePair,
    remark?: ITriggerValuePair,
    dob?: ITriggerValuePair,
    phone?: ITriggerValuePair,
    city?: ITriggerValuePair,
    state?: ITriggerValuePair,
    country?: ITriggerValuePair,
    pincode?: ITriggerValuePair,
    gender?: ITriggerValuePair,
    photoId?: ITriggerValuePair,
    submitButton?: ITriggerValuePair
}

interface ITriggerValuePair {
    value?: string,
    trigger?: boolean,
    defaultValue?: string,
    required?: boolean
}

export function getTriggerValuePair(name?: string, trigger?: boolean, defaultValue: string = "", required: boolean = false) {

    let results: ITriggerValuePair = {};
    results.value = name;
    results.trigger = trigger;
    results.defaultValue = defaultValue;
    results.required = required;

    return results;
}

export let basicTitlesForForm = {
    firstName: "First Name",
    lastName: "Last Name",
    emailId: "Email Id",
    password: "Password",
    confirmPassword: "Confirm Password",
    remark: "Remark",
    dob: "Date of Birth",
    submitButton: "Submit",
    phone: "Phone",
    city: "City",
    state: "State",
    country: "Country",
    pincode: "Pin Code",
    photoId: "Profile Photo",
    gender: "Gender",
}

export function getFormGroup(showFormFields: IShowFormFields) {
    let formGroup: FormGroup = {} as FormGroup;
    for (const key of Object.keys(showFormFields)) {
        if (showFormFields[key] && showFormFields[key].trigger === true) {
            if (showFormFields[key].required) {
                formGroup[key] = [showFormFields[key].defaultValue, Validators.required];
            }
            else {
                formGroup[key] = [showFormFields[key].defaultValue];
            }
        }
    }
    return formGroup;
}