import { Component, Input, OnInit } from '@angular/core';
import { AuthenticatedUserService } from '../../../../services/authentication/authenticated-user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent {

  @Input()
  headerUser: boolean;

  constructor(private userAuthService: AuthenticatedUserService) { }

}
