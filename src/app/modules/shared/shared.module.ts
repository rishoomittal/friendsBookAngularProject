import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as fromComponents from './components';
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from 'ng-recaptcha';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NewUserService } from '../../services/newUser/new-user.service';
import { AuthenticatedUserService } from '../../services/authentication/authenticated-user.service';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MatProgressSpinnerModule, MatSpinner } from '@angular/material/progress-spinner';
import { FriendRequestService } from '../../services/friendRequests/friend-request.service';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [...fromComponents.components],
  imports: [
    CommonModule, SharedRoutingModule, ReactiveFormsModule, MatInputModule,
    FormsModule, RecaptchaV3Module, MatDialogModule, MatFormFieldModule, 
    MatButtonModule, MatTooltipModule, MatDatepickerModule, 
    MatNativeDateModule, HttpClientModule, MatProgressSpinnerModule
  ],
  exports: [
    ...fromComponents.components,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    HeaderComponent,
    FooterComponent,
    MatProgressSpinnerModule
  ],
  providers: [
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: "<YOUR_SITE_KEY>" },
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } },
    NewUserService, AuthenticatedUserService, FriendRequestService
  ],
})
export class SharedModule { }
