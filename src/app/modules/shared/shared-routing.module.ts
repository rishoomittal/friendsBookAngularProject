import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { userRoutes } from '../user/user-routing.module';

const routesShared: Routes = [
];

@NgModule({
  imports: [RouterModule.forChild(routesShared)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
