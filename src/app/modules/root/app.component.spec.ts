import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AuthenticatedUserService } from 'src/app/services/authentication/authenticated-user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { noop } from 'rxjs';
import { HeaderComponent } from '../shared/components/header/header.component'
import { HeaderNewUserComponent } from '../shared/components/header-new-user/header-new-user.component';

describe('AppComponent', () => {


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        HeaderNewUserComponent
      ],
      providers: [
        {
          provide: AuthenticatedUserService, useValue: {
            isLoggedIn: noop,
          }
        }
      ]
    }).compileComponents();

  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'friendsBookProject'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('friendsBookProject');
  });

  it('should create the header', () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });

  it('should create the new user header', () => {
    const fixture = TestBed.createComponent(HeaderNewUserComponent);
    const headerNewUser = fixture.componentInstance;
    expect(headerNewUser).toBeTruthy();
  });
});
