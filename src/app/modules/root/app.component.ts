import { AfterViewChecked, ChangeDetectorRef, ElementRef } from '@angular/core';
import { Component } from '@angular/core';
import { AuthenticatedUserService } from '../../services/authentication/authenticated-user.service';
import { FriendRequestService } from '../../services/friendRequests/friend-request.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements AfterViewChecked {

  title = 'friendsBookProject';
  headerUser = this.userAuthenticationService.isLoggedIn();

  constructor(private userAuthenticationService: AuthenticatedUserService, private cdRef: ChangeDetectorRef, private friendService: FriendRequestService) {
    this.friendService.allFriendRequests = null;
  }

  ngAfterViewChecked(): void {
    this.userAuthenticationService.isLoggedIn() ? this.headerUser = true : this.headerUser = false;
  }
}
