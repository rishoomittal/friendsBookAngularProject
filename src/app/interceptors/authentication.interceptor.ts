import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {


        const token = localStorage.getItem("token");
        const bearer = 'Bearer ' + token;

        if (token) {
            req = req.clone({
                setHeaders: {
                    'Authorization': bearer,
                }
            });

            return next.handle(req).pipe(
                tap(event => {
                    if (event instanceof HttpResponse) {
                    }
                }, error => {
                    console.log(error);
                })
            );
        }
        else
            return next.handle(req);
    }
}