import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortByDate'
})
export class SortByDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const sortedValues = value.sort((a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime());
    return sortedValues;
  }

}
