import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { allUsersUrl } from 'src/app/constants/urlConstants';
import { Observable } from 'rxjs';
import { IPostUser } from 'src/app/modules/user/interfaces/user';
import { AuthenticatedUserService } from '../authentication/authenticated-user.service';
import { ServiceUtilities } from '../serviceUtilities';

@Injectable({
  providedIn: 'root'
})
export class AdminUtilitiesService {

  constructor(private http: HttpClient, private userAuthService: AuthenticatedUserService) { }

  fetchAllUsersInSystemAsAdmin() {
    return this.userAuthService.fetchAllUsersInSystem();
  }

  blockOrActivateUser(id: string, activateUser: boolean) {
    let user: IPostUser = {
      isActive: false
    };
    user.isActive = activateUser;
    return this.adminUpdateUserStatuses(id, user);
  }

  makeUserAdmin(id: string, makeAdmin: boolean) {
    let user: IPostUser = {
      isAdmin: false
    };
    user.isAdmin = makeAdmin;
    return this.adminUpdateUserStatuses(id, user);
  }

  private adminUpdateUserStatuses(id: string, user: IPostUser) {
    const updateUserUrl = allUsersUrl + id;
    return ServiceUtilities.sendPutRequest(this.http, updateUserUrl, user);
  }
}
