import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { allFriendRequestsUrl, createFriendRequestUrl } from 'src/app/constants/urlConstants';
import { IFriendRequest } from 'src/app/modules/user/interfaces/frinedRequest';
import { IPostUser, IUserLoggedIn } from 'src/app/modules/user/interfaces/user';
import { ServiceUtilities } from '../serviceUtilities';

@Injectable({
  providedIn: 'root'
})
export class FriendRequestService {

  allFriendRequests: IFriendRequest[] = [];

  constructor(private http: HttpClient) { }

  sendFriendRequest(idOfFriend: string, userId: string, status: string): Observable<{message: string}> {
    const createFriendReqestData: IFriendRequest = {
      userId: userId,
      friendId: idOfFriend,
      status: status
    }
    return ServiceUtilities.sendPostRequest(this.http, createFriendRequestUrl, createFriendReqestData);
  }

  updateFriendRequest(friendRequestId: string, idOfFriend: string, userId: string, updateStatus: string): Observable< {} | HttpResponse<{}>> {

    const updateFriendReqestData = {
      userId: userId,
      friendId: idOfFriend,
      status: updateStatus
    }
    const updateRequestURl = allFriendRequestsUrl + friendRequestId;
    return ServiceUtilities.sendPutRequest(this.http, updateRequestURl, updateFriendReqestData);
  }

  getFriendRequestById(friendRequestId: string) {

  }

  public getAllFriendRequestsInSystem(): Observable<IFriendRequest[]> {

    const observableObject: Observable<IFriendRequest[]> = ServiceUtilities.sendGetRequest(this.http, allFriendRequestsUrl)
    observableObject.subscribe((data) => {
      this.allFriendRequests = data;
    });
    return observableObject;
  }

  public getAllFriendRequestsAssociatedToUser(userId: string): IFriendRequest[] {
    let allFriendRequestsAssociated: IFriendRequest[] = [];

    for (let friendRequest of this.allFriendRequests) {
      if (friendRequest.userId == userId) {
        allFriendRequestsAssociated.push(friendRequest);
      }
    }
    return allFriendRequestsAssociated;
  }

  public getAllFriendIdAssociatedToUser(userId: string): string[] {
    let allIdOfFriendsOfUser: string[] = [];

    for (let friendRequest of this.allFriendRequests) {
      if (friendRequest.userId == userId) {
        allIdOfFriendsOfUser.push(friendRequest.friendId);
      }
    }
    return allIdOfFriendsOfUser;
  }
}
