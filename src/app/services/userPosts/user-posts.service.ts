import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { allPostsUrl, createPostUrl, postByUserIdUrl, serverBaseUrl, updateManyPostsUrl } from 'src/app/constants/urlConstants';
import { IPostByAUser, IUserLoggedIn } from 'src/app/modules/user/interfaces/user';
import { AuthenticatedUserService } from '../authentication/authenticated-user.service';
import { ServiceUtilities } from '../serviceUtilities';

@Injectable({
  providedIn: 'root'
})
export class UserPostsService {

  constructor(private userService: AuthenticatedUserService, private http: HttpClient) { }

  createNewPost(newPost: IPostByAUser): Observable<{message?: string}> | null {
    return ServiceUtilities.sendPostRequest(this.http, createPostUrl, newPost);
  }

  getSepecificPost(postId: string): Observable<IPostByAUser> {
    const specificPostUrl: string = allPostsUrl + postId;
    return ServiceUtilities.sendGetRequest(this.http, specificPostUrl);

  }

  getAllPostsByAUser(userId: string): Observable<IPostByAUser[]> {
    return ServiceUtilities.sendPostRequest(this.http, postByUserIdUrl, { id: userId });
  }

  getAllPostsInSystem(): Observable<IPostByAUser[]> {
    return ServiceUtilities.sendGetRequest(this.http, allPostsUrl);
  }

  async updateUserPhotoIdInAllThePosts(userPhotoId: string, userId: string): Promise<boolean> {
    const updateData = {
      "photoId": userPhotoId,
      "userId": userId
    }
    const updateResponse: {ok?: number} = await ServiceUtilities.sendPostRequest(this.http, updateManyPostsUrl, updateData).toPromise();
    if (updateResponse.ok === 1) {
      return true;
    }
    return false;
  }

  updateASpecificPost(updatePost: IPostByAUser) {
    const updatePostUrl = allPostsUrl + updatePost._id;
    return ServiceUtilities.sendPutRequest(this.http, updatePostUrl, updatePost);
  }

  async deletePost(postId: string) {
    const deletePostUrl = allPostsUrl + postId;
    return await ServiceUtilities.sendDeleteRequest(this.http, deletePostUrl).toPromise();
  }
}
