import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IPostUser, IUser, IUserLoggedIn } from 'src/app/modules/user/interfaces/user';
import { allFilesUrl, allUsersUrl, authenticateUserUrl, uploadFileUrl } from 'src/app/constants/urlConstants';
import { ServiceUtilities } from '../serviceUtilities';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedUserService {

  loggedInUser: IPostUser;

  constructor(private http: HttpClient, public jwtHelper: JwtHelperService) {
  }

  /**
   * Helps to authenticate the user
   * @param email - the email id of user
   * @param password - password of the user
   * @returns - the user object
   */
  authenticateUser(email: string, password: string): Observable<IPostUser> {

    const authenticationObject: {
      "email": string,
      "password": string
    } = {
      "email": email,
      "password": password
    };

    return ServiceUtilities.sendPostRequest(this.http, authenticateUserUrl, authenticationObject);
  }

  /**
   * Sets the users session in the local storage
   * @param loggedInUser - the logged in user object received from authenticateUser(..)
   */
  setLoggedInUserSession(loggedInUser: IPostUser) {
    this.loggedInUser = loggedInUser;
    this.setSession(this.loggedInUser.token);
  }

  /**
   * Sets the needed user variables in the session
   * @param token - to set sessio, the user token is passed
   */
  private setSession(token) {
    localStorage.setItem('token', token);
    localStorage.setItem('expires_at', this.jwtHelper.getTokenExpirationDate(token).toISOString());
    localStorage.setItem('isAdmin', this.loggedInUser.isAdmin.toString());
    localStorage.setItem('loggedInUser', JSON.stringify(this.loggedInUser));
  }

  /**
   * The user photo id is set in the local storage
   * @param photoId - The photoId of the iser
   */
  setUserPhotoIdInLocalStorage(photoId: string) {
    localStorage.removeItem("photoId");
    localStorage.setItem('photoId', photoId);
  }

  /**
   * The user photo blob is set
   * @param userPhotoBlob - The value of the blob
   */
  setUserPhotoBlobInLocalStorage(userPhotoBlob: string) {
    localStorage.removeItem("userPhotoBlob");
    localStorage.setItem('userPhotoBlob', userPhotoBlob);
  }

  /**
   * Logout the user and destroy the session
   */
  logout() {
    localStorage.removeItem("isAdmin");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("photoId");
    localStorage.removeItem("userPhotoBlob");
    localStorage.removeItem("token");
    localStorage.removeItem("loggedInUser");
  }

  /**
   * Check if the user is loggedIn
   */
  public isLoggedIn(): boolean {
    return this.getToken() ? !this.jwtHelper.isTokenExpired(this.getToken()) : false;
  }

  /**
   * Check if the user is logged out
   * @returns 
   */
  isLoggedOut(): boolean {
    return !this.isLoggedIn();
  }

  /**
   * Gets the jwt token from local storage
   */
  getToken(): string {
    return localStorage.getItem("token");
  }

  /**
   * Gets photo id from the local storage
   */
  getPhotoIdFromStorage() {
    return localStorage.getItem("photoId");
  }

  /**
   * Gets the photo blob from the storage
   */
  getPhotoBlobFromStorage() {
    return localStorage.getItem("userPhotoBlob");
  }

  /**
   * Get user is admin value from local storage
   */
  getUserIsAdminFromStorage() {
    return localStorage.getItem("isAdmin");
  }

  /**
   * Helps to check if the user is admin
   */
  checkIfUserIsAdmin(user: IUserLoggedIn) {
    return (user && user.isAdmin) ? true : false;
  }

  /**
   * If user is logged in, fetches and returns all the users present in the system
   */
  fetchAllUsersInSystem(): Observable<IPostUser[]> | null {
      return this.getLoggedInUser() !== null ? ServiceUtilities.sendGetRequest(this.http, allUsersUrl) : null;
  }

  /**
   * Helps to get logged in user details from the local storage
   */
  getLoggedInUser(): IUserLoggedIn {
    return this.isLoggedIn() ? JSON.parse(localStorage.getItem('loggedInUser')) : null;
  }

  /**
   * Helps to get the user by id
   */
  getUserById(userId: string): Observable<IPostUser> {
    const userUrl = allUsersUrl + userId;
    return ServiceUtilities.sendGetRequest<IPostUser>(this.http, userUrl);
  }

  /**
   * Helps to update the user details
   */
  updateUserDetails(updateUser: IUserLoggedIn): Observable<IUser | HttpResponse<IUser>> {
    const id = this.getLoggedInUser()._id;
    const updateUserUrl = allUsersUrl + id
    return ServiceUtilities.sendPutRequest(this.http, updateUserUrl, updateUser);
  }

  /**
   * Helps to update the password of the user
   */
  updateUserPassword(newPassword: string): Observable<{}> {
    const id = this.getLoggedInUser()._id;
    const updateUserUrl = allUsersUrl + id
    return ServiceUtilities.sendPutRequest(this.http, updateUserUrl, { password: newPassword });
  }

  public uploadImage(image: File): Observable<string> {

    /**
     * Helps to upload the image of the user
     */
    const formData = new FormData();
    formData.append('picture', image);
    return ServiceUtilities.sendPostRequest(this.http, uploadFileUrl, formData);

  }

  /**
   * Helps to get the users image
   */
  public getUserImage(userImageId: string): Observable<Blob> {
    const getImageUrl = allFilesUrl + userImageId;
    return this.http.get(getImageUrl, { responseType: "blob" }).pipe(
      catchError(ServiceUtilities.handleError)
    );
  }
}


