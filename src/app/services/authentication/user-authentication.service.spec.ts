import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { noop } from 'rxjs';

import { AuthenticatedUserService } from './authenticated-user.service';

describe('UserAuthenticationService', () => {
  let service: AuthenticatedUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{provide: AuthenticatedUserService, useValue: {
        isLoggedIn: noop,
        getLoggedInUser: () => {
          return {_id : 123};
        }
      }}]
    });
    service = TestBed.inject(AuthenticatedUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
