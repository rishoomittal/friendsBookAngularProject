import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IPostUser, IUser } from 'src/app/modules/user/interfaces/user';
import { serverBaseUrl, uploadFileUrl } from 'src/app/constants/urlConstants';
import { ServiceUtilities } from '../serviceUtilities';

@Injectable({
  providedIn: 'root'
})
export class NewUserService {

  constructor(private http: HttpClient) { }

  createUser(newUser: IUser): Observable<{message: string}> {

    const registerUserUrl = serverBaseUrl + "users/register";
    return ServiceUtilities.sendPostRequest(this.http, registerUserUrl, newUser);
  }

  public uploadImage(image: File): Observable<string> {
    const formData = new FormData();
    formData.append('picture', image);
    const imageId: Observable<string> = ServiceUtilities.sendPostRequest(this.http, uploadFileUrl, formData);
    return imageId;
  }
}


