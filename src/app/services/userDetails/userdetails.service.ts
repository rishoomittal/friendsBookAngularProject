import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { allUsersUrl } from 'src/app/constants/urlConstants';
import { IPostUser, IUserLoggedIn } from 'src/app/modules/user/interfaces/user';
import { ServiceUtilities } from '../serviceUtilities';

@Injectable({
  providedIn: 'root'
})
export class UserdetailsService {

  constructor(private http: HttpClient) { }

  getUserById(userId: string) {

  }

  getUserByEmail(email: string) {

  }

  uploadUserProfilePhoto() {

  }

  getUserProfilePhoto() {

  }

  /*updateUserDetails(user: IUserLoggedIn){
    const url = allUsersUrl + user._id;
    const updatedUser = ServiceUtilities.sendPostRequest(this.http,url,user);
    updatedUser.subscribe
    (response => {
      console.log(response.status);
    });
  }*/

  updateUsersPhotoId() {

  }

  updateUserName(firstName, lastName) {

  }
}
