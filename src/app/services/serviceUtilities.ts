import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

export class ServiceUtilities {

  public static sendPostRequest<typeOfData,returnType>(httpClient: HttpClient, url: string, data: typeOfData): Observable<returnType> {
    return httpClient.post<returnType>(url, data)
      .pipe(
        catchError(ServiceUtilities.handleError)
      );
  }

  public static sendGetRequest<returnType>(httpClient: HttpClient, url: string, response?: string, header?: any): Observable<returnType> {
    return httpClient.get<returnType>(url).pipe(
      catchError(ServiceUtilities.handleError)
    );
  }

  public static sendPutRequest<typeOfData,returnType>(httpClient: HttpClient, url: string, data: typeOfData, header?: any): Observable<returnType | HttpResponse<returnType>> {
    return httpClient.put<returnType>(url, data, { observe: 'response' })
      .pipe(
        catchError(ServiceUtilities.handleError)
      );
  }

  public static sendDeleteRequest<returnType>(httpClient: HttpClient, url: string, header?: any): Observable<returnType>{
    return httpClient.delete<returnType>(url).pipe(
      catchError(ServiceUtilities.handleError)
    );
  }

  public static handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}

