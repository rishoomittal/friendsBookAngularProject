import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { FriendRequestService } from 'src/app/services/friendRequests/friend-request.service';
import { IPostUser } from 'src/app/modules/user/interfaces/user';
import { IFriendRequest } from '../modules/user/interfaces/frinedRequest';

@Injectable()
export class AllFriendRequestResolver implements Resolve<Observable<IFriendRequest[]>> {

    constructor(private friendService: FriendRequestService) { }

    resolve() {
        return this.friendService.getAllFriendRequestsInSystem().pipe(
            take(10),
            catchError(error => {
                return of(error);
            }));
    }
}