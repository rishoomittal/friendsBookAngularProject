import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IPostUser } from 'src/app/modules/user/interfaces/user';
import { AdminUtilitiesService } from '../services/adminUtilities/admin-utilities.service';

@Injectable()
export class UserListResolve implements Resolve<Observable<IPostUser[]>> {

  constructor(private adminUtilitiesService: AdminUtilitiesService) { }

  resolve() {
    return this.adminUtilitiesService.fetchAllUsersInSystemAsAdmin().pipe(
      catchError(error => {
        return of(error);
      }));
  }
}