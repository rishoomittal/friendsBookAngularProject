import { readFile } from 'fs/promises';
const compilerOptions = JSON.parse(
  await readFile(
    new URL('./tsconfig.json', import.meta.url)
  )
);

export default {

  testPathIgnorePatterns: [
    "./node_modules/",
    "./dist/"
  ],
  moduleDirectories: [
    "./node_modules/",
    "./src/"
  ],
  moduleFileExtensions: ['ts', 'html', 'js', 'json', 'mjs'],
  globals: {
    "ts-jest": {
      "tsConfig": "./tsconfig.spec.json",
      "stringifyContentPathRegex": "\\.html$"
    }
  },
  transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
  preset: 'jest-preset-angular',
  resolver: 'jest-preset-angular/build/resolvers/ng-jest-resolver.js',
  transform: {
    '^.+\\.(ts|js|mjs|html|svg)$': 'jest-preset-angular',
  },
  roots: ['./src/'],
  testMatch: ['**/+(*.)+(spec).+(ts)'],
  setupFilesAfterEnv: ['./src/setupJest.ts'],
  collectCoverage: true,
  coverageReporters: ['html'],
  coverageDirectory: 'coverage/friendsBookAngularProject',
  modulePaths: [
    "./", "./src/", "./node_modules/"
  ],
  "moduleNameMapper": {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "spec/mocks/*.js",
    "\\.(css|less)$": "spec/mocks/*.js"
  }
}